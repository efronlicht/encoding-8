use super::{ascii, ebcdic};
#[test]
fn bijection() {
    (0_u8..=255).for_each(|b| assert_eq!(b, ascii::to_ebcdic(ebcdic::to_ascii(b))));
}
