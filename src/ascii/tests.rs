use super::*;

#[test]

fn test_cases() {
    for b in 0..=255 {
        match b {
            b'a'..=b'z' | b'A'..=b'Z' => {
                assert_ne!(to_lowercase(b), to_uppercase(b));
                assert_eq!(to_lowercase(to_uppercase(b)), to_lowercase(b));
                assert_eq!(to_uppercase(to_lowercase(b)), to_uppercase(b));
                assert_eq!(to_lowercase(to_uppercase(b)), to_lowercase(b));
            }
            _ => {
                assert_eq!(to_lowercase(b), to_uppercase(b));
                assert_eq!(b, to_lowercase(b))
            }
        }
    }
}
#[test]
fn test_checks() {
    for b in 0..=255 {
        match b {
            0..=31 | DEL => {
                assert!(is_control(b), "{}", b as char);
                assert!(!is_alphanumeric(b), "{}", b as char);
            }
            b'a'..=b'z' => {
                assert!(is_alphanumeric(b), "{}", b as char);
                assert!(is_alphabetical(b), "{}", b as char);
                assert!(is_lowercase(b), "{}", b as char);
                assert!(is_printable(b), "{}", b as char);
            }
            b'A'..=b'Z' => {
                assert!(is_alphanumeric(b), "{}", b as char);
                assert!(is_alphabetical(b), "{}", b as char);
                assert!(is_uppercase(b), "{}", b as char);
                assert!(is_printable(b), "{}", b as char);
            }
            b'0'..=b'9' => {
                assert!(is_alphanumeric(b), "{}", b as char);
                assert!(is_printable(b), "{}", b as char);
                assert!(is_numeric(b), "{}", b as char);
            }
            _ => {
                assert!(is_printable(b), "{}", b as char);
                assert!(!is_alphanumeric(b), "{}", b as char);
            }
        };
    }
}
